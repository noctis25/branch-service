package com.branch.exercise.service;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import com.branch.exercise.config.BranchConfiguration;
import com.branch.exercise.model.User;
import java.util.Objects;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(classes = {BranchConfiguration.class})
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@EnableAutoConfiguration
public class UserServiceTest {
  @Autowired
  private TestRestTemplate template;

  @Test
  public void shouldReturn404WhenUserExists() {
    final ResponseEntity<User> response = template.getForEntity("https://api.github.com/users/elementanno", User.class);
    assertThat(Objects.requireNonNull(response.getBody()).displayName()).isNull();

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
  }

  @Test
  public void shouldReturn404WhenUserDoesNotExist() {
    final ResponseEntity<User> response = template.getForEntity("https://api.github.com/users/octocat", User.class);
    assertThat(Objects.requireNonNull(response.getBody()).displayName()).isEqualTo("The Octocat");

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

}
