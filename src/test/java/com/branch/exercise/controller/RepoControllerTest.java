package com.branch.exercise.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.asyncDispatch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.request;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.hamcrest.core.StringContains.containsString;
import static org.mockito.Mockito.when;

import com.branch.exercise.model.UserBuilder;
import com.branch.exercise.service.UserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

@ExtendWith(SpringExtension.class)
@WebMvcTest(value = RepoController.class)
public class RepoControllerTest {

  @MockBean
  private UserService userService;

  private static final UserBuilder userBuilder = UserBuilder.anUser().withDisplayName("Octocat");

  @Autowired
  private MockMvc mockMvc;

  @Autowired
  private RepoController controller;

  @Test
  public void contextLoads() throws Exception {
    assertThat(controller).isNotNull();
  }

  @Test
  public void greetingShouldReturnMessageFromService() throws Exception {
    when(userService.getByUsername("octocat")).thenReturn(userBuilder.build());

    MvcResult mvcResult = mockMvc.perform(get("/api/v1/repo/octocat"))
        .andExpect(request().asyncStarted())
        .andDo(MockMvcResultHandlers.log())
        .andReturn();

    mockMvc.perform(asyncDispatch(mvcResult))
        .andExpect(status().isOk())
        .andExpect(content().string(containsString("\"display_name\" : \"Octocat\"")));
  }
}