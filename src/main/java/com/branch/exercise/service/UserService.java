package com.branch.exercise.service;

import com.branch.exercise.exceptions.ResourceNotFoundException;
import com.branch.exercise.model.Repo;
import com.branch.exercise.model.User;
import java.net.URI;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Service
public class UserService {

  private static final String GITHUB_API_URL = "https://api.github.com";
  private static final String GITHUB_API_PATH = "users";
  private final RestTemplate restTemplate;

  @Autowired
  public UserService(RestTemplate restTemplate) {
    this.restTemplate = restTemplate;
  }

  /**
   * Returns a User that matches a valid username.
   *
   * @param username Valid username from github
   * @return A User object appended to a Repo collection that matches the user
   */
  @Async
  @Cacheable("users")
  public User getByUsername(String username) {
    List<Repo> repos = getRepo(username);
    User user = getUser(username);

    return user.withRepo(repos);
  }

  /**
   * Explicit GET request from github public API only for a valid user,to construct the User object
   *
   * @param username Valid username from github
   * @return A User object
   */
  private User getUser(String username) {
    return CompletableFuture.supplyAsync(() -> {
      try {
        URI endpoint = UriComponentsBuilder.fromUriString
                (GITHUB_API_URL).path(GITHUB_API_PATH)
            .pathSegment(username).build().toUri();

        restTemplate.getForObject(endpoint, User.class);

        return restTemplate.getForObject(endpoint, User.class);
      } catch (HttpClientErrorException e) {
        throw new ResourceNotFoundException("User " + username + " - " + "Not Found!");
      }
    }).join();
  }

  /**
   * Explicit GET request from github public API only for a valid user,to construct the Repo object
   *
   * @param username
   * @return
   */
  private List<Repo> getRepo(String username) {
    return CompletableFuture.supplyAsync(() -> {
      try {
        URI endpoint2 = UriComponentsBuilder.fromUriString
                (GITHUB_API_URL).path(GITHUB_API_PATH)
            .pathSegment(username).pathSegment("repos").build().toUri();

        return List.of(Objects.requireNonNull(restTemplate.getForObject(endpoint2, Repo[].class)));
      } catch (HttpClientErrorException e) {
        throw new ResourceNotFoundException("User " + username + " - " + "Not Found!");
      }
    }).join();
  }
}
