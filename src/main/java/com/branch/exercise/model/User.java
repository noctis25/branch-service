package com.branch.exercise.model;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import java.time.LocalDateTime;
import java.util.List;

@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public record User(@JsonProperty("user_name") @JsonAlias({"login"}) String login,
                   @JsonProperty("display_name") @JsonAlias({"name"}) String displayName,
                   @JsonProperty("avatar") @JsonAlias({"avatar_url"}) String avatar,
                   @JsonProperty("geo_location") @JsonAlias({"location"}) String geoLocation,
                   @JsonProperty("email") String email,
                   @JsonProperty("url") @JsonAlias({"html_url"})String url,
                   @JsonProperty("created_at") LocalDateTime createdAt,
                   @JsonAlias({"repo"}) List<Repo> repo) {

  public User withRepo(List<Repo> repo) {
    return new User(login(), displayName(), avatar(), geoLocation(), email(), url(), createdAt(),
        repo);
  }

}
