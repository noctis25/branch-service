package com.branch.exercise.model;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.LocalDateTime;
import java.util.List;

public final class UserBuilder {

  private @JsonProperty("user_name")
  @JsonAlias({"login"}) String login;
  private @JsonProperty("display_name")
  @JsonAlias({"name"}) String displayName;
  private @JsonProperty("avatar")
  @JsonAlias({"avatar_url"}) String avatar;
  private @JsonProperty("geo_location")
  @JsonAlias({"location"}) String geoLocation;
  private @JsonProperty("email") String email;
  private @JsonProperty("url")
  @JsonAlias({"html_url"}) String url;
  private @JsonProperty("created_at") LocalDateTime createdAt;
  private @JsonAlias({"repo"}) List<Repo> repo;

  private UserBuilder() {
  }

  public static UserBuilder anUser() {
    return new UserBuilder();
  }

  public UserBuilder withLogin(String login) {
    this.login = login;
    return this;
  }

  public UserBuilder withDisplayName(String displayName) {
    this.displayName = displayName;
    return this;
  }

  public UserBuilder withAvatar(String avatar) {
    this.avatar = avatar;
    return this;
  }

  public UserBuilder withGeoLocation(String geoLocation) {
    this.geoLocation = geoLocation;
    return this;
  }

  public UserBuilder withEmail(String email) {
    this.email = email;
    return this;
  }

  public UserBuilder withUrl(String url) {
    this.url = url;
    return this;
  }

  public UserBuilder withCreatedAt(LocalDateTime createdAt) {
    this.createdAt = createdAt;
    return this;
  }

  public UserBuilder withRepo(List<Repo> repo) {
    this.repo = repo;
    return this;
  }

  public User build() {
    return new User(login, displayName, avatar, geoLocation, email, url, createdAt, repo);
  }
}
