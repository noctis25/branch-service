package com.branch.exercise.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class MyExceptionHandler {

  @ExceptionHandler(ResourceNotFoundException.class)
  @ResponseStatus(HttpStatus.NOT_FOUND)
  public ResponseEntity<ExceptionResponse> handleException(ResourceNotFoundException e) {
    return ResponseEntity
        .status(HttpStatus.NOT_FOUND)
        .body(new ExceptionResponse(e.getMessage(), HttpStatus.NOT_FOUND.value()));
  }

  @ExceptionHandler(value = IllegalArgumentException.class)
  public ResponseEntity<ExceptionResponse> illegalArgumentExceptionHandler(
      IllegalArgumentException e) {
    return ResponseEntity
        .status(HttpStatus.NOT_ACCEPTABLE)
        .body(new ExceptionResponse(e.getMessage(), HttpStatus.NOT_ACCEPTABLE.value()));
  }


  public record ExceptionResponse(String message, Integer code) {

  }
}