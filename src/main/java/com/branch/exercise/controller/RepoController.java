package com.branch.exercise.controller;

import com.branch.exercise.service.UserService;
import java.util.concurrent.CompletableFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1")
public class RepoController {

  @Autowired
  UserService userService;

  @GetMapping("/repo/{username}")
  public CompletableFuture<ResponseEntity> getRepos(
      @PathVariable String username) {
    return CompletableFuture.supplyAsync(() -> userService.getByUsername(username))
        .thenApply(ResponseEntity::ok);
  }
}
