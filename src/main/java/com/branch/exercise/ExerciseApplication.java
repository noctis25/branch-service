package com.branch.exercise;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class ExerciseApplication {

  /**
   * Simple Spring boot REST API to get user data from Github public API's
   * @param args
   */
  public static void main(String[] args) {
    SpringApplication.run(ExerciseApplication.class, args);
  }
}