Branch Service
===================  

This API was initialzied using spring boot

Below is the REST API endpoint available:

```
GET /repo/{username}
```
This endpoint will query the github public API and get some necessary properties to build a new object for us that joins the basic user data with their repo data

Note: username should exist in github, or this will return a '404'

## How to Run

This application is packaged as a war which has Tomcat 8 embedded. No Tomcat or JBoss installation is necessary. You run it using the ```java -jar``` command.

* Clone this repository
* Make sure you are using JDK 1.8 and Maven 3.x
* You can build the project and run the tests by running ```mvn clean package```
* Once successfully built, you can run the service using this method:
```
java -jar target/exercise-0.0.1-SNAPSHOT.jar
```
This service was developed rapidly, in ~2.5 hours utilizing the spring initializr ``https://start.spring.io/``

It allows you to bootstrap a REST API with very minimal configuration, and provides us an embedded tomcat server to run this app

With the time I had, I decided to create:

* A configuration file to handle object mapping and caching
* A User builder for the user record, to cut down unnecessary construction 
* A custom exception handler and exception class
* A service to handle external requests and tests for the controller and service
